package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    
    HashMap<String, Double> prices = new HashMap<>();

    public Quoter() {
        prices.put("1", 10.0);
        prices.put("2", 45.0);
        prices.put("3", 20.0);
        prices.put("4", 35.0);
        prices.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        for (Map.Entry<String, Double> key : prices.entrySet()) {
            if (key.getKey().equals(isbn)) {
                return key.getValue();
            }
        }
        return 0.0;
    }
}
